import {useEffect} from "react";
import {useSelector, useDispatch} from "react-redux";
import {useHistory} from "react-router-dom";
import {ErrorMessage, Field, Form, Formik} from "formik";
import * as Yup from "yup";

import Loading from "../components/Loading";
import Api from "../api";
import {logIn, setLoginError} from "../store/actions/sessionActions";
import {RootStore} from "../store/reducers";

type LoginData = {
  email: string;
  password: string;
}

const initialFormValue = {
  email: '',
  password: '',
} as LoginData

const LoginSchema = Yup.object().shape({
  email: Yup.string().email('Invalid email').required('Email is required'),
  password: Yup.string().required('Password is required'),
})

function LoginPage () {
  const dispatch = useDispatch()
  const history = useHistory()
  const session = useSelector((state: RootStore) => state.session)

  useEffect(() => {
    if (session.token) {
      history.push("/home")
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [session])

  return <div className={"container container--login container--decor"}>
    <div className="container__edge-left" />
    <div className="container__edge-right" />

    <div className="logo" />

    <Formik<LoginData>
      initialValues={initialFormValue}
      validationSchema={LoginSchema}
      onSubmit={async (values) => {
        try {
          const {token} = await Api.login(values.email, values.password);

          if (token) {
            localStorage.setItem("jwt", token)
            dispatch(logIn(token));
          }
        } catch (error) {
          dispatch(setLoginError(error.message));
        }
      }}
    >
      {({ isSubmitting}) => (
        <Form className="form form--login">
          <h2 className="header-2">Sign in</h2>

          <div className="form-field">
            <div className="form-field__icon">
              <i className="icon-email" />
            </div>
            <Field
              className="form-field__input"
              type="email"
              name="email"
              placeholder="Email" />
            <ErrorMessage name="email" component="div" className="form-field__error-msg" />
          </div>

          <div className="form-field">
            <div className="form-field__icon">
              <i className="icon-lock" />
            </div>
            <Field
              className="form-field__input"
              type="password"
              name="password"
              placeholder="Password" />
            <ErrorMessage name="password" component="div" className="form-field__error-msg" />
          </div>

          <div className="form-field">
            <div className="form-field__error-msg form-field__error-msg--main-error">{session.error}</div>
          </div>

          <button className="form__submit" type="submit" disabled={isSubmitting}>
            {isSubmitting ? <Loading className={"loading-ring--sm"} /> : <p className="text">Save</p>}
          </button>
        </Form>
      )}
    </Formik>
  </div>
}

export default LoginPage

