import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";

import SideBar from "../components/SideBar";
import Search from "../components/Search";
import UserCard from "../components/UserCard";
import UserForm from "../components/UserForm";
import Loading from "../components/Loading";
import {RootStore} from "../store/reducers";
import {loadUsers} from "../store/actions/usersActions";

function HomePage () {
  const dispatch = useDispatch()
  const {users, isLoading} = useSelector((state: RootStore) => state.users)
  const [filter, setFilter] = useState("")
  const [isFormOpen, setFormOpen] = useState(false)

  useEffect(() => {
    dispatch(loadUsers())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return <main>
    <div className={"top-bar"}>
      <div className="logo" />
    </div>

    <SideBar />

    <div className={`overlay ${isFormOpen ? 'overlay--show' : ''}`}>
      <UserForm closeForm={() => setFormOpen(false)} />
    </div>

    <div className="main-container">
      <div className="header">
        <h3 className="header-1">Organization Users</h3>

        <Search onChange={setFilter} />
      </div>

      <div className="card-list">
        {isLoading && <Loading />}
        {users && users
          .filter(user => user.FirstName.includes(filter) || user.LastName.includes(filter))
          .map(user => <UserCard key={user.ID} user={user} />)}
      </div>

      <button onClick={() => setFormOpen(true)} className={"btn-icon btn-add-user"}>
        <i className={"icon-plus"}/>
      </button>
    </div>
  </main>
}

export default HomePage

