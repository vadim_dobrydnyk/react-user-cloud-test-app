import React from "react";
import { useField, useFormikContext } from "formik";
// @ts-ignore
import DatePicker from "react-datepicker";

interface Props {
  name: string,
  placeholderText: string
}

function DatePickerField({ ...props }: Props) {
  const { setFieldValue } = useFormikContext();
  const [field] = useField(props);
  return (
    <DatePicker
      {...field}
      {...props}
      selected={(field.value && new Date(field.value)) || null}
      onChange={(val: string) => {
        setFieldValue(field.name, val)
      }}
    />
  );
};

export default DatePickerField
