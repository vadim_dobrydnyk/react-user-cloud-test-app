interface Props {
  className?: string
}

function Loading({className}: Props) {
  return <div className={`loading-ring ${className}`}>
    <div />
    <div />
    <div />
    <div />
  </div>
}

export default Loading
