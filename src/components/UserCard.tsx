import {useDispatch} from "react-redux";

import {User} from "../store/reducers/userReducer";
import {deleteUser} from "../store/actions/usersActions";

interface Props {
  user: User
}

function UserCard({user}: Props) {
  const dispatch = useDispatch()

  const onDelete = () => dispatch(deleteUser(user.objectId))

  return (
    <div className="user-card">
      <div className="user-card__avatar">
        <img src={user.Photo} alt="user avatar" />
      </div>

      <div className="user-card__title">
        <h3 className="header-3">
          {user.FirstName} {user.LastName}
        </h3>
        <p className="text">{user.Role}</p>
      </div>

      <div className="user-card__info">
        <p className="text">ID: {user.ID}</p>
        <p className="text">BirthDate: {new Date(Number(user.BirthDate)).toUTCString()}</p>
        <p className="text">Address: {user.Address}</p>
      </div>

      <div className="user-card__controls">
        <i className="icon-email-bg" />
        <p className="text">{user.Email}</p>
        <button className={"btn-icon"} onClick={onDelete}>
          <i className={"icon-trash"} />
        </button>
      </div>
    </div>
  )
}

export default UserCard
