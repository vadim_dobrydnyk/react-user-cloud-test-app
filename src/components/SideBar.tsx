import {useDispatch} from "react-redux";
import {logOut} from "../store/actions/sessionActions";


function SideBar () {
  const dispatch = useDispatch()

  const logOutHandler = () => {
    localStorage.removeItem("jwt");
    dispatch(logOut())
  }

  return (
    <div className="side-bar">
      <button className={"side-bar__open btn-icon"}>
        <i className={"icon-arrow"} />
      </button>

      <div className="menu">
        <button className={"btn-icon btn-menu"}>
          <i className={"icon-user"} />
        </button>
      </div>

      <button onClick={logOutHandler} className={"btn-icon btn-logout"}>
        <i className={"icon-logout"} />
      </button>
    </div>
  )
}

export default SideBar
