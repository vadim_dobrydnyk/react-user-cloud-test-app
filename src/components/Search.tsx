import {Dispatch, SetStateAction, useState} from "react";

import {useDebouncedEffect} from "../customHooks/useDebouncedEffect";

interface Props {
  onChange: Dispatch<SetStateAction<string>>
}

function Search ({onChange}: Props) {
  const [value, setValue] = useState("")

  useDebouncedEffect(() => onChange(value), 200, [value])

  return (
    <div className="search-input">
      <div className="search-input__decor-corners" />
      <div className="search-input__decor-bg" />

      <i className="icon-search" />
      <input
        className="search-input__input"
        type="text"
        value={value}
        onChange={(e) => setValue(e.target.value)}
        placeholder="Search for a user" />
    </div>
  )
}

export default Search;
