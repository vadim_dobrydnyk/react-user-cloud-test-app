import {useDispatch} from "react-redux";
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import DatePickerField from "./DatePicker";
import Loading from "./Loading";
import Api from "../api";
import {User} from "../store/reducers/userReducer";
import {mapToUserDTO, toBase64} from "../utils";
import * as types from "../store/actions/actionTypes";
import {useAppConfig} from "../customHooks/useAppConfig";

const initialFormValue = {
  ID: '',
  Photo: '',
  FirstName: '',
  LastName: '',
  BirthDate: '',
  Email: '',
  Address: '',
  Role: '',
} as User

const UserSchema = Yup.object().shape({
  ID: Yup.string().required('ID is required'),
  Photo: Yup.string().required('ID is required'),
  FirstName: Yup.string().max(25).required('First name is required'),
  LastName: Yup.string().max(25).required('Last name is required'),
  BirthDate: Yup.string().required('Birth date is required'),
  Email: Yup.string().email('Invalid email').required('Email is required'),
  Address: Yup.string().max(250).required('Address is required'),
  Role: Yup.string().required('Role is required'),
})

interface Props {
  closeForm: () => void
}

function UserForm({closeForm}: Props) {
  const dispatch = useDispatch();
  const { config: { roles } } = useAppConfig()

  let resetFormikForm = () => {}

  const closeResetForm = () => {
    closeForm()
    resetFormikForm()
  }

  return (
    <div className="user-form">
      <button onClick={closeResetForm} className="btn-icon"><i className="icon-close" /></button>

      <Formik<User>
        initialValues={initialFormValue}
        validationSchema={UserSchema}
        onSubmit={async (values, { setSubmitting, setFieldError }) => {
          setSubmitting(true);

          try {
            const createdUser = await Api.createUser(mapToUserDTO(values))
            closeResetForm()
            dispatch({ type: types.USER_CREATED, payload: createdUser})
          } catch (e) {
            const [error] = e.message.split(".")
            for(const [key] of Object.entries(initialFormValue)) {
              if (error.includes(key)) {
                setFieldError(key, error)
              }
            }
          } finally {
            setSubmitting(false);
          }
        }}
      >
        {({ isSubmitting, setFieldValue, resetForm}) => {
          resetFormikForm = resetForm

          return (
            <Form className="form form--user">
              <h2 className="header-2">New user</h2>

              <div className="form-field">
                <Field
                  className="form-field__input"
                  type="text"
                  name="FirstName"
                  placeholder="First name" />
                <ErrorMessage name="FirstName" component="div" className="form-field__error-msg" />
              </div>

              <div className="form-field">
                <Field
                  className="form-field__input"
                  type="text"
                  name="LastName"
                  placeholder="Last name" />
                <ErrorMessage name="LastName" component="div" className="form-field__error-msg" />
              </div>

              <div className="form-field">
                <Field
                  className="form-field__input"
                  type="email"
                  name="Email"
                  placeholder="Email" />
                <ErrorMessage name="Email" component="div" className="form-field__error-msg" />
              </div>

              <div className="form-field">
                <Field
                  className="form-field__input"
                  type="text"
                  name="ID"
                  placeholder="ID" />
                <ErrorMessage name="ID" component="div" className="form-field__error-msg" />
              </div>

              <div className="form-field form-field--birthdate">
                <DatePickerField name="BirthDate" placeholderText="Birth date" />
                <ErrorMessage name="BirthDate" component="div" className="form-field__error-msg" />
              </div>

              <div className="form-field">
                <Field
                  className="form-field__input"
                  type="text"
                  name="Address"
                  placeholder="Address" />
                <ErrorMessage name="Address" component="div" className="form-field__error-msg" />
              </div>

              <div className="form-field">
                <Field
                  as="select"
                  className="form-field__select"
                  name="Role"
                  placeholder="Role"
                >
                  {roles && roles.map(role => <option key={role} value={role}>{role}</option>)}
                </Field>
                <ErrorMessage name="Role" component="div" className="form-field__error-msg" />
              </div>

              <div className="form-field">
                <input
                  className="form-field__input"
                  type="file"
                  name="Photo"
                  onChange={async (event) => {
                    if (event.target && event.target.files) {
                      const photoBase64 = await toBase64(event.target.files[0])
                      setFieldValue("Photo", photoBase64);
                    }
                  }}/>
                <ErrorMessage name="Photo" component="div" className="form-field__error-msg" />
              </div>

              <button className="form__submit" type="submit" disabled={isSubmitting}>
                {isSubmitting ? <Loading className={"loading-ring--sm"} /> : <p className="text">Save</p>}
              </button>
            </Form>
          )
        }}
      </Formik>
    </div>
  )
}

export default UserForm
