import {User, UserDTO} from "./store/reducers/userReducer";

export function mapToUserDTO(user: User): UserDTO {
  return {
    ID: Number(user.ID),
    Photo: user.Photo,
    FirstName: user.FirstName,
    LastName: user.LastName,
    BirthDate: Number(user.BirthDate),
    Email: user.Email,
    Address: user.Address,
    Role: user.Role
  } as UserDTO
}

export const toBase64 = (file: File) => new Promise((resolve, reject) => {
  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = () => resolve(reader.result);
  reader.onerror = error => reject(error);
});
