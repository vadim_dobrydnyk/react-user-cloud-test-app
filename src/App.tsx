import {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {
  BrowserRouter as Router,
  Route, Redirect
} from "react-router-dom";

import LoginPage from "./pages/LoginPage";
import HomePage from "./pages/HomePage";
import {RootStore} from "./store/reducers";
import {logIn} from "./store/actions/sessionActions";


function App() {
  const dispatch = useDispatch()
  const session = useSelector((state: RootStore) => state.session)

  useEffect(() => {
    const jwt = localStorage.getItem("jwt")

    if (jwt) {
      dispatch(logIn(jwt))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Router>
      <Route exact path={"/"} component={LoginPage} />
      <Route exact path="/home">
        {session.token ? <HomePage /> : <Redirect to="/" /> }
      </Route>
    </Router>
  );
}

export default App;
