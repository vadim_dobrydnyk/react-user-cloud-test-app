import {useEffect, useState} from "react";
import Api from "../api";

export type AppConfig = {
  roles: string[],
  theme: string | undefined
}

const initConfig: AppConfig = {
  roles: [],
  theme: undefined
}

export function useAppConfig() {
  const [config, setConfig] = useState<AppConfig>(initConfig)

  useEffect(() => {
    const fetchData = async () => {
      const data = await Api.getConfig()

      const enums: {role: string[]} = JSON.parse(data.Enums)
      const config: {theme: string} = JSON.parse(data.Config)

      setConfig({
        roles: enums.role,
        theme: config.theme
      })
    }

    fetchData()
  }, [])

  return {config}
}

