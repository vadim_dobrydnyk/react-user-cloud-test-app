import * as types from "./actionTypes";

export function logIn(token: string) {
  return { type: types.SET_TOKEN, payload: token };
}

export function logOut() {
  return { type: types.SET_TOKEN, payload: "" };
}

export function setLoginError(errorMsg: string) {
  return { type: types.SET_ERROR, payload: errorMsg };
}
