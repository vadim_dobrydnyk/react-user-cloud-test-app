import Api from '../../api';
import * as types from './actionTypes';
import {User} from "../reducers/userReducer";

export function loadUsersSuccess(users: User[]) {
  return { type: types.USERS_LOAD_SUCCESS, payload: users};
}

export function loadUsersFailure(error: string) {
  return { type: types.USERS_LOAD_FAILURE, payload: error };
}

export function loadUsers() {
  return async function(dispatch: any) {
    dispatch({ type: types.USERS_LOAD })

    try {
      const users = await Api.getUsers();

      dispatch(loadUsersSuccess(users));
    } catch (error) {
      dispatch(loadUsersFailure(error));
    }
  }
}


export function deleteUserSuccess(id: string) {
  return { type: types.USER_DELETE_SUCCESS, payload: id};
}

export function deleteUser(id: string) {
  return async function(dispatch: any) {
    dispatch({ type: types.USER_DELETE })

    try {
      await Api.deleteUser(id);

      dispatch(deleteUserSuccess(id));
    } catch (error) {
      dispatch(loadUsersFailure(error));
    }
  }
}
