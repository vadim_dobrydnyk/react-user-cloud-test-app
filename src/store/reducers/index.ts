import { combineReducers } from 'redux'

import session, {SessionStore} from './sessionReducer';
import users, {UserStore} from './userReducer';

export type RootStore = {
  session: SessionStore,
  users: UserStore
}

export default combineReducers({
  session,
  users,
});
