import * as types from '../actions/actionTypes';

export type User = {
  ID: string
  Photo: string
  FirstName: string
  LastName: string
  BirthDate: string
  Email: string
  Address: string
  Role: string
  objectId: string
}

export type UserDTO = {
  ID: number
  Photo: string
  FirstName: string
  LastName: string
  BirthDate: number
  Email: string
  Address: string
  Role: string
}

export interface UserStore {
  users: User[],
  error: boolean,
  isLoading: boolean
}

const initialState: UserStore = {
  users: [],
  error: false,
  isLoading: false,
};

export default function userReducer(state = initialState, action: {type: string, payload: any}) {
  switch (action.type) {
    case types.USERS_LOAD:
      return {
        ...state,
        isLoading: true
      }

    case types.USERS_LOAD_SUCCESS:
      return {
        ...state,
        users: action.payload,
        isLoading: false
      }

    case types.USERS_LOAD_FAILURE:
      return {
        ...state,
        error: true,
        isLoading: false
      }

    case types.USER_DELETE_SUCCESS:
      const id = action.payload
      return {
        ...state,
        users: state.users.filter(user => user.objectId !== id)
      }

    case types.USER_CREATED:
      const user = action.payload
      return {
        ...state,
        users: [...state.users, user]
      }

    default:
      return state;
  }
}
