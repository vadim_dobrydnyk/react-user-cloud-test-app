import * as types from '../actions/actionTypes';

export interface SessionStore {
  token: string,
  error: string
}

const initialState: SessionStore = {
  token: "",
  error: ""
};

export default function sessionReducer(state = initialState, action: {type: string, payload: string}) {
  switch (action.type) {
    case types.SET_TOKEN:
      return {
        ...state,
        token: action.payload
      };

    case types.SET_ERROR:
      return {
        ...state,
        error: action.payload
      };

    default:
      return state;
  }
}
