import {User, UserDTO} from "./store/reducers/userReducer";

class Api {
  static authHost = "https://reqres.in"
  static usersUrl = "https://api.backendless.com/525C6ECF-BE35-D74F-FFC7-777E0FA29F00/3777A415-DFD8-4240-B51A-DCE496757AB7/data/cyberhat_users"
  static configUrl = "https://api.backendless.com/525C6ECF-BE35-D74F-FFC7-777E0FA29F00/3777A415-DFD8-4240-B51A-DCE496757AB7/data/cyberhat_config/7C908D2B-DA78-4C9B-8BFF-36647A42E86D"

  static apiFetch<T>(request: any): Promise<T> {
    return fetch(request)
      .then(response => Promise.all([response.json(), response.ok]))
      .then(([body, isOk]) => {
        if (!isOk) {
          throw new Error(body.message || body.error || "Request Error")
        }

        return body
      })
  }

  static createUser(user: UserDTO): Promise<Response> {
    const request = new Request(this.usersUrl,  {
      method: "POST",
      headers: new Headers({ "Content-Type": "application/json" }),
      body: JSON.stringify(user)
    })

    return this.apiFetch(request)
  }

  static getUsers(): Promise<User[]> {
    const request = new Request(this.usersUrl, {
      method: "GET"
    })

    return this.apiFetch<User[]>(request)
  }

  static deleteUser(id: string) {
    const request = new Request(`${this.usersUrl}/${id}`, {
      method: "DELETE",
      headers: new Headers({ "Content-Type": "application/json" })
    })

    return this.apiFetch(request)
  }

  static login(email: string, password: string): Promise<{token: string}> {
    const request = new Request(this.authHost + `/api/login`, {
      method: "POST",
      headers: new Headers({ "Content-Type": "application/json" }),
      body: JSON.stringify({email, password})
    })

    return this.apiFetch<{token: string}>(request)
  }

  static getConfig(): Promise<{Enums: string, Config: string}> {
    const request = new Request(this.configUrl, {
      method: "GET"
    })

    return this.apiFetch<{Enums: string, Config: string}>(request)
  }
}

export default Api;
